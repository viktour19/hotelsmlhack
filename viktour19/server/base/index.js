// Base routes for default index/root path, about page, 404 error pages, and others..

hotelscontroller  = require('./../controllers/hotelscontroller');

exports.register = function(server, options, next){

    server.route([

        {
            method: 'GET',
            path: '/',
            config: {
                handler: hotelscontroller.prototype.newalgo,
                id: 'newalgo'
            }
        },

        {
            method: 'GET',
            path: '/predict',
            config: {
                handler: hotelscontroller.prototype.predict,
                id: 'predict'
            }
        },

        {
            method: 'GET',
            path: '/{path*}',
            config: {
                handler: function(request, reply){
                    reply.view('404', {
                        title: 'Total Bummer 404 Page'
                    }).code(404);
                },
                id: '404'
            }
        }

    ]);

    next();
}

exports.register.attributes = {
    name: 'base'
};