"use strict";
var convnetjs = require('convnetjs');
var svm = require('node-svm');
var so = require('stringify-object');
var Q = require('q');
var fs = require('fs');
var path = require('path');

module.exports = hotelscontroller;

var datalist = 0, size = 0;
var inputlist = Array();
var data = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 0];
var dataarray = Array();
var dataarray2 = Array();
var dataarray3 = Array();
var net;
var netpath = path.join(__dirname, 'datasets/hotelsnet.json');
var modelpath = path.join(__dirname, 'datasets/hotelsmodel.json');
var dummypath = path.join(__dirname, 'datasets/hotelsdummy.json');

function hotelscontroller() {

}

function check(field, type) {
    if (typeof field === 'undefined') {
        return false;
    }
    else {
        return true;
    }
}

function count(checkstring, matchchar) {
    var count = 0;
    for (var i = 0; i < checkstring.length; i++) {
        if (checkstring[i] === matchchar)
            count++;


    }
    return count;
}

hotelscontroller.prototype =
{

    newalgo: function (request, reply) {
        
        var dataarraycopy = Array();

        var resultobj =
            {
                "totalprediction": 0,
                "totalcancelled": 0,
                "totalnotcancelled": 0,
                "predictedcancelled": 0,
                "predictednotcancelled": 0,
                "predictedcancelledwrong": 0,
                "predictedcancelledcorrect": 0,
                "predictednotcancelledwrong": 0,
                "predictednotcancelledcorrect": 0,
                "percentageofcancelledcorrecttocancelled": 0
            }
        request.server.plugins['hapi-mysql'].pool.getConnection(function (err, connection) {

            connection.query(
                'SELECT MAX(id) as max , count(id) as count FROM bookings',
                function (err, rows) {
                    var st = check(rows, 0);

                    if (st) {
                        st = check(rows[0].max, 0);
                        if (st) {
                            size = rows[0].max;

                            datalist = new Array(size);
                            inputlist = new Array(size + 1);

                        }
                    }

                });

            connection.query(
                'SELECT *  FROM bookings',
                function (err, rows) {

                    var length = 0;
                    var st = check(rows, 0);

                    if (st) {
                        datalist = rows;
                        length = rows.length;
                    }


                    for (var i = 0; i < length; i++) {
                        if (check(datalist[i].id, 0)) {

                            var inputfields
                                =
                                {
                                    "mob": 0,
                                    "outsideng": 0,
                                    "srm": 0,
                                    "phonebk": 0,
                                    "cab": 0,
                                    "ipgtone": 0,
                                    "weekday": 0,
                                    "priorbk": 0,
                                    "vbkbefore": 0,
                                    "vfraudulent": 0,
                                    "roomgtone": 0,
                                    "roomltfive": 0,
                                    "cancelledbefore": 0,
                                    "id": 0,
                                    "iscancelled": 0

                                };

                            var row = datalist[i];
                            var booking_id = row.id;
                            inputfields.iscancelled = row.cancelled; //todo remove this when pushing
                            inputfields.id = booking_id;
                            inputfields.cab = row.cab_service_req;
                            inputfields.mob = row.made_on_behalf;
                            inputfields.srm = row.special_request_made;
                            inputfields.phonebk = row.is_phone_booking;

                            var date = new Date(row.time);
                            var day = date.getDay();

                            inputfields.weekday = (day > 0 && day < 6) ? 1 : 0;

                            inputfields.outsideng = (row.country == 'Nigeria') ? 0 : 1;
                            inputfields.roomgtone = (row.number_of_rooms > 1) ? 1 : 0;
                            inputfields.roomltfive = (row.number_of_rooms < 5) ? 1 : 0;

                            //mob,outsideng,srm,phonebk,cab,weekday,roomgtone,roomtltfive,id

                            var ip = row.ip_address;

                            if (ip != '' && count(ip, '.') == 3) {
                                var ipcount = 0;
                                for (var iip = 0; iip < length; iip++) {
                                    if (ip === datalist[iip].ip_address) {
                                        ipcount++;
                                    }
                                }

                                inputfields.ipgtone = (ipcount > 1) ? 1 : 0
                            }
                            //ipgtone


                            var email = row.email_address;

                            var bookedbeforeandnotcancelledcount = 0, bookedbeforecount = 0, bookedbeforeandcancelled = 0;

                            for (var iemail = 0; iemail < length; iemail++) {
                                if (email === datalist[iemail].email_address && booking_id != datalist[iemail].id) {
                                    bookedbeforecount++
                                    if (datalist[iemail].iscancelled == '0')

                                        bookedbeforeandnotcancelledcount++;

                                    else if (datalist[iemail].iscancelled == '1')
                                        bookedbeforeandcancelled++;


                                }
                                inputfields.priorbk = (bookedbeforecount > 0) ? 1 : 0;
                                inputfields.vbkbefore = (bookedbeforeandnotcancelledcount > 0) ? 1 : 0;
                                inputfields.cancelledbefore = (bookedbeforeandcancelled > 0) ? 1 : 0;
                               
                            }

                            //train svm with input fields
                            //train cnn with input fields
                            //get probablilites of of cancellations

                            inputlist[booking_id] = inputfields;


                        }


                    }

                    var mobcount = 0, ousidengcount = 0, weekdaycount = 0, phonebkcount = 0, cabcount = 0, ipgtonecount = 0
                        , vbkbeforecount = 0, vfraudulentcount = 0, roomgtonecount = 0, roomltfivecount = 0, cancelledbeforecount = 0, priorbkcount = 0
                        , srmcount = 0;
                    if (check(inputlist[1], 0)) {
                        for (var j = 1; j < inputlist.length; j++) {
                            if (inputlist[j].mob == 1) mobcount++;
                            if (inputlist[j].outsideng == 1) ousidengcount++;
                            if (inputlist[j].weekday == 1) weekdaycount++;
                            if (inputlist[j].phonebk == 1) phonebkcount++;
                            if (inputlist[j].cab == 1) cabcount++;
                            if (inputlist[j].ipgtone == 1) ipgtonecount++;
                            if (inputlist[j].vbkbefore == 1) vbkbeforecount++;
                            if (inputlist[j].vfraudulent == 1) vfraudulentcount++;
                            if (inputlist[j].roomgtone == 1) roomgtonecount++;
                            if (inputlist[j].roomltfive == 1) roomltfivecount++;
                            if (inputlist[j].cancelledbefore == 1) cancelledbeforecount++;
                            if (inputlist[j].priorbk == 1) priorbkcount++;
                            if (inputlist[j].srm == 1) srmcount++;

                        }
                    }

                    var mobcountc = 0, ousidengcountc = 0, weekdaycountc = 0, phonebkcountc = 0, cabcountc = 0, ipgtonecountc = 0
                        , vbkbeforecountc = 0, vfraudulentcountc = 0, roomgtonecountc = 0, roomltfivecountc = 0, cancelledbeforecountc = 0, priorbkcountc = 0
                        , srmcountc = 0, caa = 0;
                    if (check(inputlist[1], 0)) {
                        for (var j = 1; j < inputlist.length; j++) {
                            if (inputlist[j].mob == 1 && inputlist[j].iscancelled == 1) mobcountc++;
                            if (inputlist[j].outsideng == 1 && inputlist[j].iscancelled == 1) ousidengcountc++;
                            if (inputlist[j].weekday == 1 && inputlist[j].iscancelled == 1) weekdaycountc++;
                            if (inputlist[j].phonebk == 1 && inputlist[j].iscancelled == 1) phonebkcountc++;
                            if (inputlist[j].cab == 1 && inputlist[j].iscancelled == 1) cabcountc++;
                            if (inputlist[j].ipgtone == 1 && inputlist[j].iscancelled == 1) ipgtonecountc++;
                            if (inputlist[j].vbkbefore == 1 && inputlist[j].iscancelled == 1) vbkbeforecountc++;
                            if (inputlist[j].vfraudulent == 1 && inputlist[j].iscancelled == 1) vfraudulentcountc++;
                            if (inputlist[j].roomgtone == 1 && inputlist[j].iscancelled == 1) roomgtonecountc++;
                            if (inputlist[j].roomltfive == 1 && inputlist[j].iscancelled == 1) roomltfivecountc++;
                            if (inputlist[j].cancelledbefore == 1 && inputlist[j].iscancelled == 1) cancelledbeforecountc++;
                            if (inputlist[j].priorbk == 1 && inputlist[j].iscancelled == 1) priorbkcountc++;
                            if (inputlist[j].srm == 1 && inputlist[j].iscancelled == 1) srmcountc++;


                        }
                    }


                    //console.log(ousidengcountc/size);
                    //console.log(mobcountc/size);
                    //console.log(weekdaycountc/size);
                    //console.log(phonebkcountc/size);
                    //console.log(cabcountc/size);
                    //console.log(ipgtonecountc/size);
                    //console.log(vbkbeforecountc/size);
                    //console.log(vfraudulentcountc/size);
                    //console.log(roomgtonecountc/size);
                    //console.log(roomltfivecountc/size);
                    //console.log(cancelledbeforecountc/size);
                    //console.log(priorbkcountc/size);
                    //console.log(srmcountc/size);


                    // var layer_defs=[];
                    //
                    //layer_defs.push({type:'input',out_sx:1,out_sy:1,out_depth:2});
                    //
                    //layer_defs.push({type:'fc',num_neurons:20,activation:'relu'});
                    //layer_defs.push({type:'fc',num_neurons:20,activation:'relu'});
                    //
                    //layer_defs.push({type:'softmax',num_classes:2});
                    //
                    //var net = new convnetjs.Net();
                    //net.makeLayers(layer_defs);
                    //
                    // var x= new convnetjs.Vol([1, 1]);
                    //
                    //
                    // var probability_volume=net.forward(x);
                    //console.log('probabilitythatxisclass 0:'+probability_volume.w[0]);
                    //
                    // var trainer= new convnetjs.Trainer(net,{learning_rate:0.01,l2_decay:0.001}); trainer.train(x,0);
                    //
                    // var probability_volume2=net.forward(x);
                    // console.log('probabilitythatxisclass 0:'+probability_volume2.w[0]);


                    //var opts =
                    //{
                    //    "train_ratio": 1, //whatportionofdatagoestotrain,intrain/validationfoldsplits.Here, 3  opts.num_folds=1;//numberoffoldstoevaluatepercandidate
                    //    "num_candidates": 50, //numberofcandidatestoevaluateinparallel
                    //    "num_epochs": 20, // //numberofepochstomakethroughdataperfold
                    //    "ensemble_size": 20 //howmanynetstoaverageintheendforprediction?likelyhigher=bette 7
                    //
                    //};
                    //
                    //
                    //var train_data = [new convnetjs.Vol([1, 3, 8]), new convnetjs.Vol([5, 9, 3]), new convnetjs.Vol([1, 0, 8]),
                    //    new convnetjs.Vol([0, 3, 2]), new convnetjs.Vol([3, 3, 9]), new convnetjs.Vol([7, 7, 8]),
                    //    new convnetjs.Vol([9, 0, 1]), new convnetjs.Vol([3, 1, 5])];
                    //
                    //var train_labels = [0, 0, 1, 0, 0, 1, 1, 0];
                    //
                    //var magicNet = new convnetjs.MagicNet(train_data, train_labels, opts);
                    //
                    //magicNet.onFinishBatch(finishedBatch);
                    //
                    //setInterval(function(){ magicNet.step() }, 0);
                    //
                    //function finishedBatch() {
                    //
                    //    var some_test_vol = new convnetjs.Vol([3, 1, 5]);
                    //    var predicted_label = magicNet.predict_soft(some_test_vol);
                    //
                    //    //console.log(predicted_label);
                    //
                    //
                    //}
                    //
                    //
                    //var wstream = fs.createWriteStream(netpath);
                    //wstream.write(so(magicNet.toJSON()));
                    //wstream.end();


                    var clf = new svm.CSVC({
                        svmType: 'C_SVC',
                        c: [0.03125, 0.125, 0.5, 2, 8],
            
                        // kernels parameters
                        kernelType: 'SIGMOID',
                        gamma: [0.03125, 0.125, 0.5, 2, 8],
                        r: [0.125,0.5,0,1],
               
            
                        // training options
                        nFold: 200,
                        normalize: true,
                        reduce: true,
                        retainedVariance: 0.99,
                        eps: 1e-3,
                        cacheSize: 2000,
                        shrinking : true,
                        probability : true
                    });

                    var caa = 0;
                    for (var i = 1; i < 2000; i++) {
                         data = new Array(2);
                        data = [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 0];
                        data[0][0] = inputlist[i].mob;
                        data[0][1] = inputlist[i].outsideng;
                        data[0][2] = inputlist[i].srm;
                        data[0][3] = inputlist[i].phonebk;
                        data[0][4] = inputlist[i].cab;
                        data[0][5] = inputlist[i].ipgtone;
                        data[0][6] = inputlist[i].weekday;
                        data[0][7] = inputlist[i].priorbk;
                        data[0][8] = inputlist[i].vbkbefore;
                        data[0][9] = inputlist[i].roomgtone;
                        data[0][10] = inputlist[i].roomltfive;
                        data[0][11] = inputlist[i].cancelledbefore;

                        data[1] = inputlist[i].iscancelled;
                        dataarray.push(data);
                    }


                    for (var i = 2000; i < length; i++) {
                        data = new Array(2);
                        data =  [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 0];
                        
                        data[0][0] = inputlist[i].mob;
                        data[0][1] = inputlist[i].outsideng;
                        data[0][2] = inputlist[i].srm;
                        data[0][3] = inputlist[i].phonebk;
                        data[0][4] = inputlist[i].cab;
                        data[0][5] = inputlist[i].ipgtone;
                        data[0][6] = inputlist[i].weekday;
                        data[0][7] = inputlist[i].priorbk;
                        data[0][8] = inputlist[i].vbkbefore;
                        data[0][9] = inputlist[i].roomgtone;
                        data[0][10] = inputlist[i].roomltfive;
                        data[0][11] = inputlist[i].cancelledbefore;
                       
                        dataarray2.push(data);
                        data[1] = inputlist[i].iscancelled;
                  
                        dataarraycopy.push(data);
                                      
                    }

                  
                    clf.train(dataarray)
                        .progress(function (progress) {
                           console.log('training progress: %d%', Math.round(progress * 100));
                        }).spread(function (trainedModel, trainingReport) {


                            var wstream = fs.createWriteStream(modelpath);
                            wstream.write(JSON.stringify(trainedModel));
                            wstream.end();

                            console.log(dataarray2[1][0]);
                            var prediction = clf.predictSync(dataarray2[1][0]);
                           
                            console.log(prediction);

                            console.log(dataarray2[2][0]);
                            prediction = clf.predictProbabilitiesSync(dataarray2[2][0]);

                            console.log(prediction);
                            

                            for (var s = 1; s < dataarray2.length; s++) {

                                if (dataarraycopy[s][1] === 1) {
                                    resultobj.totalcancelled++;
                                }

                                if (dataarraycopy[s][1] === 0) {
                                    resultobj.totalnotcancelled++;
                                }
                                var top = dataarray2[s][0];

                                prediction = clf.predictSync(top);

                                //console.log(prediction);

                                resultobj.totalprediction++;

                                //console.log('%d, %d, %d, %d, %d, %d , %d, %d, %d , %d, %d => %d', datarraycopy[s][0][0], datarraycopy[s][0][1], datarraycopy[s][0][2], datarraycopy[s][0][3], datarraycopy[s][0][4], datarraycopy[s][0][5], datarraycopy[s][0][6], datarraycopy[s][0][7], datarraycopy[s][0][8], datarraycopy[s][0][9], datarraycopy[s][0][10], datarraycopy[s][0][11], prediction);

                                dataarray2[s][1] = prediction;

                                if (prediction === 1) {
                                    resultobj.predictedcancelled++;
                                    if (dataarraycopy[s][1] == 1) {
                                        resultobj.predictedcancelledcorrect++;
                                    }
                                    if (dataarraycopy[s][1] == 0) {
                                        resultobj.predictedcancelledwrong++;
                                    }
                                }
                                if (prediction == 0) {
                                    resultobj.predictednotcancelled++;
                                    if (dataarraycopy[s][1] == 1) {
                                        resultobj.predictednotcancelledwrong++;
                                    }
                                    if (dataarraycopy[s][1] == 0) {
                                        resultobj.predictednotcancelledcorrect++;
                                    }
                                }

                            }
                            
                            resultobj.percentageofcancelledcorrecttocancelled = (resultobj.predictedcancelledcorrect/resultobj.totalcancelled)* 100;
                            console.log(resultobj);
                        });

                    //
                    ////console.log(dataarray2);
                    //svm.read(dummypath)
                    //    .then(function(dataset){
                    //        return clf.train(dataarray);
                    //    })
                    //    .then(function(trainedModel, trainingReport){
                    //        return svm.read(dummypath);
                    //    })
                    //    .then(function(testset){
                    //        return clf.evaluate(dataarray2);
                    //    })
                    //.done(function(report){
                    //    console.log(report);
                    //});


                }
                )


            connection.release();
        });



        reply.view('hotelsresult', {
            title: 'Result',
            result: resultobj
        }).code(404);


    },

    predict: function (request, reply) {
        
        var dataarraycopy  = Array();
        
        //
        //var net = new convnetjs.Net();
        //
        //var wstream = fs.createReadStream(netpath);
        //
        //net.fromJSON(wstream.read());
        //wstream.end();
        //
        //var x= new convnetjs.Vol([0.5, -1.3])
        //
        //
        //var probability_volume=net.forward(x);
        //console.log('probabilitythatxisclass 0:'+probability_volume.w[0]);

        var resultobj =
            {
                "totalprediction": 0,
                "totalcancelled": 0,
                "totalnotcancelled": 0,
                "predictedcancelled": 0,
                "predictednotcancelled": 0,
                "predictedcancelledwrong": 0,
                "predictedcancelledcorrect": 0,
                "predictednotcancelledwrong": 0,
                "predictednotcancelledcorrect": 0,
                "percentageofcancelledcorrecttocancelled": 0
            }

        request.server.plugins['hapi-mysql'].pool.getConnection(function (err, connection) {

            connection.query(
                'SELECT *  FROM bookings',
                function (err, rows) {

                    var length = 0;
                    var st = check(rows, 0);

                    if (st) {
                        datalist = rows;
                        length = rows.length;
                    }


                    for (var i = 0; i < length; i++) {
                        if (check(datalist[i].id, 0)) {

                            var inputfields
                                =
                                {
                                    "mob": 0,
                                    "outsideng": 0,
                                    "srm": 0,
                                    "phonebk": 0,
                                    "cab": 0,
                                    "ipgtone": 0,
                                    "weekday": 0,
                                    "priorbk": 0,
                                    "vbkbefore": 0,
                                    "vfraudulent": 0,
                                    "roomgtone": 0,
                                    "roomltfive": 0,
                                    "cancelledbefore": 0,
                                    "id": 0,
                                    "iscancelled": 0

                                };

                            var row = datalist[i];
                            var booking_id = row.id;

                            inputfields.id = booking_id;
                            inputfields.cab = row.cab_service_req;
                            inputfields.mob = row.made_on_behalf;
                            inputfields.srm = row.special_request_made;
                            inputfields.phonebk = row.is_phone_booking;

                            var date = new Date(row.time);
                            var day = date.getDay();

                            inputfields.weekday = (day > 0 && day < 6) ? 1 : 0;

                            inputfields.outsideng = (row.country == 'Nigeria') ? 0 : 1;
                            inputfields.roomgtone = (row.number_of_rooms > 1) ? 1 : 0;
                            inputfields.roomltfive = (row.number_of_rooms < 5) ? 1 : 0;

                            //mob,outsideng,srm,phonebk,cab,weekday,roomgtone,roomtltfive,id

                            var ip = row.ip_address;

                            if (ip != '' && count(ip, '.') == 3) {
                                var ipcount = 0;
                                for (var iip = 0; iip < length; iip++) {
                                    if (ip === datalist[iip].ip_address) {
                                        ipcount++;
                                    }
                                }

                                inputfields.ipgtone = (ipcount > 1) ? 1 : 0
                            }
                            //ipgtone


                            var email = row.email_address;

                            var bookedbeforeandnotcancelledcount = 0, bookedbeforecount = 0, bookedbeforeandcancelled = 0;

                            for (var iemail = 0; iemail < length; iemail++) {
                                if (email === datalist[iemail].email_address && booking_id != datalist[iemail].id) {
                                    bookedbeforecount++
                                    if (datalist[iemail].iscancelled == '0')

                                        bookedbeforeandnotcancelledcount++;

                                    else if (datalist[iemail].iscancelled == '1')
                                        bookedbeforeandcancelled++;


                                }
                                inputfields.priorbk = (bookedbeforecount > 0) ? 1 : 0;
                                inputfields.vbkbefore = (bookedbeforeandnotcancelledcount > 0) ? 1 : 0;
                                inputfields.cancelledbefore = (bookedbeforeandcancelled > 0) ? 1 : 0;
                            }


                            inputlist[booking_id] = inputfields;


                        }


                    }

                    var mobcount = 0, ousidengcount = 0, weekdaycount = 0, phonebkcount = 0, cabcount = 0, ipgtonecount = 0
                        , vbkbeforecount = 0, vfraudulentcount = 0, roomgtonecount = 0, roomltfivecount = 0, cancelledbeforecount = 0, priorbkcount = 0
                        , srmcount = 0;
                    if (check(inputlist[1], 0)) {
                        for (var j = 1; j < inputlist.length; j++) {
                            if (inputlist[j].mob == 1) mobcount++;
                            if (inputlist[j].outsideng == 1) ousidengcount++;
                            if (inputlist[j].weekday == 1) weekdaycount++;
                            if (inputlist[j].phonebk == 1) phonebkcount++;
                            if (inputlist[j].cab == 1) cabcount++;
                            if (inputlist[j].ipgtone == 1) ipgtonecount++;
                            if (inputlist[j].vbkbefore == 1) vbkbeforecount++;
                            if (inputlist[j].vfraudulent == 1) vfraudulentcount++;
                            if (inputlist[j].roomgtone == 1) roomgtonecount++;
                            if (inputlist[j].roomltfive == 1) roomltfivecount++;
                            if (inputlist[j].cancelledbefore == 1) cancelledbeforecount++;
                            if (inputlist[j].priorbk == 1) priorbkcount++;
                            if (inputlist[j].srm == 1) srmcount++;

                        }
                    }

                    var mobcountc = 0, ousidengcountc = 0, weekdaycountc = 0, phonebkcountc = 0, cabcountc = 0, ipgtonecountc = 0
                        , vbkbeforecountc = 0, vfraudulentcountc = 0, roomgtonecountc = 0, roomltfivecountc = 0, cancelledbeforecountc = 0, priorbkcountc = 0
                        , srmcountc = 0, caa = 0;
                    if (check(inputlist[1], 0)) {
                        for (var j = 1; j < inputlist.length; j++) {
                            if (inputlist[j].mob == 1 && inputlist[j].iscancelled == 1) mobcountc++;
                            if (inputlist[j].outsideng == 1 && inputlist[j].iscancelled == 1) ousidengcountc++;
                            if (inputlist[j].weekday == 1 && inputlist[j].iscancelled == 1) weekdaycountc++;
                            if (inputlist[j].phonebk == 1 && inputlist[j].iscancelled == 1) phonebkcountc++;
                            if (inputlist[j].cab == 1 && inputlist[j].iscancelled == 1) cabcountc++;
                            if (inputlist[j].ipgtone == 1 && inputlist[j].iscancelled == 1) ipgtonecountc++;
                            if (inputlist[j].vbkbefore == 1 && inputlist[j].iscancelled == 1) vbkbeforecountc++;
                            if (inputlist[j].vfraudulent == 1 && inputlist[j].iscancelled == 1) vfraudulentcountc++;
                            if (inputlist[j].roomgtone == 1 && inputlist[j].iscancelled == 1) roomgtonecountc++;
                            if (inputlist[j].roomltfive == 1 && inputlist[j].iscancelled == 1) roomltfivecountc++;
                            if (inputlist[j].cancelledbefore == 1 && inputlist[j].iscancelled == 1) cancelledbeforecountc++;
                            if (inputlist[j].priorbk == 1 && inputlist[j].iscancelled == 1) priorbkcountc++;
                            if (inputlist[j].srm == 1 && inputlist[j].iscancelled == 1) srmcountc++;


                        }
                    }


                    //console.log(ousidengcountc/size);
                    //console.log(mobcountc/size);
                    //console.log(weekdaycountc/size);
                    //console.log(phonebkcountc/size);
                    //console.log(cabcountc/size);
                    //console.log(ipgtonecountc/size);
                    //console.log(vbkbeforecountc/size);
                    //console.log(vfraudulentcountc/size);
                    //console.log(roomgtonecountc/size);
                    //console.log(roomltfivecountc/size);
                    //console.log(cancelledbeforecountc/size);
                    //console.log(priorbkcountc/size);
                    //console.log(srmcountc/size);


                    // var layer_defs=[];
                    //
                    //layer_defs.push({type:'input',out_sx:1,out_sy:1,out_depth:2});
                    //
                    //layer_defs.push({type:'fc',num_neurons:20,activation:'relu'});
                    //layer_defs.push({type:'fc',num_neurons:20,activation:'relu'});
                    //
                    //layer_defs.push({type:'softmax',num_classes:2});
                    //
                    //var net = new convnetjs.Net();
                    //net.makeLayers(layer_defs);
                    //
                    // var x= new convnetjs.Vol([1, 1]);
                    //
                    //
                    // var probability_volume=net.forward(x);
                    //console.log('probabilitythatxisclass 0:'+probability_volume.w[0]);
                    //
                    // var trainer= new convnetjs.Trainer(net,{learning_rate:0.01,l2_decay:0.001}); trainer.train(x,0);
                    //
                    // var probability_volume2=net.forward(x);
                    // console.log('probabilitythatxisclass 0:'+probability_volume2.w[0]);


                    //var opts =
                    //{
                    //    "train_ratio": 1, //whatportionofdatagoestotrain,intrain/validationfoldsplits.Here, 3  opts.num_folds=1;//numberoffoldstoevaluatepercandidate
                    //    "num_candidates": 50, //numberofcandidatestoevaluateinparallel
                    //    "num_epochs": 20, // //numberofepochstomakethroughdataperfold
                    //    "ensemble_size": 20 //howmanynetstoaverageintheendforprediction?likelyhigher=bette 7
                    //
                    //};
                    //
                    //
                    //var train_data = [new convnetjs.Vol([1, 3, 8]), new convnetjs.Vol([5, 9, 3]), new convnetjs.Vol([1, 0, 8]),
                    //    new convnetjs.Vol([0, 3, 2]), new convnetjs.Vol([3, 3, 9]), new convnetjs.Vol([7, 7, 8]),
                    //    new convnetjs.Vol([9, 0, 1]), new convnetjs.Vol([3, 1, 5])];
                    //
                    //var train_labels = [0, 0, 1, 0, 0, 1, 1, 0];
                    //
                    //var magicNet = new convnetjs.MagicNet(train_data, train_labels, opts);
                    //
                    //magicNet.onFinishBatch(finishedBatch);
                    //
                    //setInterval(function(){ magicNet.step() }, 0);
                    //
                    //function finishedBatch() {
                    //
                    //    var some_test_vol = new convnetjs.Vol([3, 1, 5]);
                    //    var predicted_label = magicNet.predict_soft(some_test_vol);
                    //
                    //    //console.log(predicted_label);
                    //
                    //
                    //}
                    //
                    //
                    //var wstream = fs.createWriteStream(netpath);
                    //wstream.write(so(magicNet.toJSON()));
                    //wstream.end();


                    for (var i = 2000; i < length; i++) {
                        data = new Array(2);
                        data =  [[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0], 0];
                        
                        
                        data[0][0] = inputlist[i].mob;
                        data[0][1] = inputlist[i].outsideng;
                        data[0][2] = inputlist[i].srm;
                        data[0][3] = inputlist[i].phonebk;
                        data[0][4] = inputlist[i].cab;
                        data[0][5] = inputlist[i].ipgtone;
                        data[0][6] = inputlist[i].weekday;
                        data[0][7] = inputlist[i].priorbk;
                        data[0][8] = inputlist[i].vbkbefore;
                        data[0][9] = inputlist[i].roomgtone;
                        data[0][10] = inputlist[i].roomltfive
                        data[0][11] = inputlist[i].cancelledbefore;

                         dataarray2.push(data);
                        data[1] = inputlist[i].iscancelled;
                  
                        dataarraycopy.push(data);
                    }


                    var persistedModel = JSON.parse(fs.readFileSync(modelpath));

                    var clf = new svm.CSVC({ model: persistedModel });

                  
                            for (var s = 1; s < dataarray2.length; s++) {

                                if (dataarraycopy[s][1] === 1) {
                                    resultobj.totalcancelled++;
                                }

                                if (dataarraycopy[s][1] === 0) {
                                    resultobj.totalnotcancelled++;
                                }
                                var top = dataarray2[s][0];

                                var prediction = clf.predictSync(top);

                                //console.log(prediction);

                                resultobj.totalprediction++;

                                //console.log('%d, %d, %d, %d, %d, %d , %d, %d, %d , %d, %d => %d', datarraycopy[s][0][0], datarraycopy[s][0][1], datarraycopy[s][0][2], datarraycopy[s][0][3], datarraycopy[s][0][4], datarraycopy[s][0][5], datarraycopy[s][0][6], datarraycopy[s][0][7], datarraycopy[s][0][8], datarraycopy[s][0][9], datarraycopy[s][0][10], datarraycopy[s][0][11], prediction);

                                dataarray2[s][1] = prediction;

                                if (prediction === 1) {
                                    resultobj.predictedcancelled++;
                                    if (dataarraycopy[s][1] == 1) {
                                        resultobj.predictedcancelledcorrect++;
                                    }
                                    if (dataarraycopy[s][1] == 0) {
                                        resultobj.predictedcancelledwrong++;
                                    }
                                }
                                if (prediction == 0) {
                                    resultobj.predictednotcancelled++;
                                    if (dataarraycopy[s][1] == 1) {
                                        resultobj.predictednotcancelledwrong++;
                                    }
                                    if (dataarraycopy[s][1] == 0) {
                                        resultobj.predictednotcancelledcorrect++;
                                    }
                                }

                            }
                            
                            resultobj.percentageofcancelledcorrecttocancelled = (resultobj.predictedcancelledcorrect/resultobj.totalcancelled)* 100;
                            console.log(resultobj);

                }
                )

            connection.release();

        });


    }

    //id time  client_title  made_on_behalf  country  email_address  phone   addtional_info
    //special_request_made hotelname amount checkin checkout cab_service_req number_of_rooms
    //is_phone_booking hotels_state  ip_address cancelled customer_cancelled_by_self


}

