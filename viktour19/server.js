/**
* Dependencies.
*/
var Hapi = require('hapi');
var jshare = require('jshare');

// Create a new server
var server = new Hapi.Server();

// Setup the server with a host and port
server.connection({
    port: parseInt(process.env.PORT, 10) || 3000,
    host: '0.0.0.0'
});

// Setup the views engine and folder
server.views({
    engines: {
        html: require('swig')
    },
    path: './server/views'
});


// Export the server to be required elsewhere.
module.exports = server;

var options =
{
    host:"localhost",
    user:"root",
    password:"",
    database:"HotelsHack"
}

server.register([
    {
        register: require("hapi-assets"),
        options: require('./assets.js')
    },
    {
        register: require("hapi-mysql"),
        options: options
    },
    {
        register: require("hapi-named-routes")
    },
    {
        register: require("hapi-cache-buster")
    },
    {
        register: require('./server/assets/index.js')
    },
    {
      register: require('./server/base/index.js')
    }
], function () {
    //Start the server
    server.start(function() {
        //Log to the console the host and port info
        console.log('Server started at: ' + server.info.uri);
        
    });
});
