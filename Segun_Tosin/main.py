__author__ = 'segunfamisa'

from sklearn import svm, preprocessing
from sklearn.neighbors import KNeighborsClassifier
from sklearn.decomposition import PCA
from sklearn.externals import joblib
from sklearn import metrics
import numpy as np
import dataset

MODEL_FILENAME = "models/model.pkl"

def normalize(X):
    normalizer = preprocessing.StandardScaler().fit(X)
    return normalizer.fit_transform(X)

def reduceDimensions(X):
    pca = PCA()
    pca.fit(X)


    return pca.fit_transform(X)

def train(X_train, y_train):
    clf = svm.SVC(kernel='rbf', gamma=20)
    clf.fit(X_train, y_train)

    return clf

def train_KNN(X_train, y_train):
    knn = KNeighborsClassifier()
    knn.fit(X_train, y_train)

    return knn


def main(fileurl="data.json", training=True):
    #load data
    df = dataset.load_raw(fileurl)

    #transform data
    X, y = dataset.transform_data(df)

    #split training set
    # X_train, y_train, X_test, y_test = dataset.split_data(X, y)

    X_train = X
    y_train = y
    #
    # #normalize data
    X_train = normalize(X_train)
    #
    # #reduce dimensions
    X_train = reduceDimensions(X_train)

    if training:
        # train model
        model = train(X_train, y_train)

        #write model to file
        joblib.dump(model, MODEL_FILENAME)
    else:
        # load model
        model = joblib.load(MODEL_FILENAME)

    #predict
    y_pred = model.predict(X_train)



    #write results
    print len(X_train)
    count = 0
    total = 0
    for i in range(len(y_pred)):
        score = ""
        if y_train[i] == 1:
            #increment total cancellation count
            total += 1
            if y_pred[i] == y_train[i]:
                #increment correct predictions
                count += 1
                score = "Correct"
            else:
                score = "Wrong"


        #print "Original y:" , y_train[i] , "Predicted y: ", y_pred[i] , score

    print total , "cancellations from original data"
    print float(count) * 100 / total , "% correct predictions"




# python main.py true url



if __name__ =='__main__':
    from sys import argv as a

    #training
    train_data = True
    fileurl = ""

    if len(a)>2:
        train_data = bool(a[2])


    fileurl = a[1]

    #hardcoded to always train with the newest data
    main(fileurl, True)

