__author__ = 'segunfamisa'


from datetime import datetime as dt


def get_hour_of_day(date_str):
   dt_obj = parse_datetime_str(date_str)
   hour = dt_obj.hour
   return hour


def get_day_of_wk(date_str):
   dt_obj = parse_datetime_str(date_str)
   hour = dt_obj.weekday()
   return hour


def get_duration_seconds(start_dt_str, end_dt_str):
   start_dt = parse_datetime_str(start_dt_str)
   end_dt = parse_datetime_str(end_dt_str)
   time_diff = end_dt - start_dt
   return abs(time_diff.total_seconds())


def parse_datetime_str(dt_str):
   dt_str = dt_str.strip()

   try:
       dat = dt.strptime(dt_str, "%Y-%m-%d %H:%M:%S")

   except ValueError:
       dt_str = "2013-01-01 00:00:00"
       dat = dt.strptime(dt_str, "%Y-%m-%d %H:%M:%S")

   return dat