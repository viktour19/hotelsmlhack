__author__ = 'segunfamisa'

import pandas as pd
import numpy as np
import utils
import json

X = []
y = []

X_test = []
y_test = []

X_train = []
y_train = []

COLUMNS = {
    0:"hour_of_day",
    1:"day_of_week",
    2:"client_title",
    3:"made_on_behalf",
    4:"country",
    5:"no_of_bookings",
    6:"no_of_cancellations",
    7:"req_made",
    8:"no_of_bookings_hotel",
    9:"no_of_cancellation_hotel",
    10:"amount",
    11:"hour_of_checkin",
    12:"day_of_week_of_checkin",
    13:"duration_of_stay",
    14:"cab_service_req",
    15:"no_of_rooms",
    16:"is_phone_booking",
    17:"hotel_state"
}

emails = {}

client_titles = {}

countries = {}

hotel_states = {}

def load_raw(url):
    df = pd.read_json(url)

    return df

# load_raw("http://cms.hotels.ng/hackml/")


def transform_data(df):
    X = [[0 for x in range(len(COLUMNS))] for x in range(len(df))]
    y = [0 for x in range(len(df))]

    for j, booking in df.iterrows():
        for i in range(len(COLUMNS)):
            #hour of day
            if i == 0 :
                X[j][i] = utils.get_hour_of_day(booking['time'])

            #day of week
            elif i == 1:
                X[j][i] = utils.get_day_of_wk(booking['time'])

            #client title
            elif i == 2:
                #use proportion of client title
                try:
                    X[j][i] = df['client_title'].value_counts()[booking['client_title']] / float(len(df['client_title']))
                except:
                    X[j][i] = 0

            #made on behalf
            elif i == 3:
                X[j][i] = booking['made_on_behalf']

            #country
            elif i == 4:
                try:
                    X[j][i] = df['country'].value_counts()[booking['country']] / float(len(df['country']))
                except:
                    X[j][i] = 0

            #email address occurences
            elif i == 5:
                X[j][i] = df['email_address'].value_counts()[booking['email_address']]

            #no of cancellations by email_address
            #implement case 6
            elif i == 6:
                try:
                    X[j][i] = len(df[(df['email_address'] == booking['email_address']) & (df['cancelled'] == 1)])
                except:
                    X[j][i] = 0

            #request_made
            elif i == 7:
                X[j][i] = booking['special_request_made']

            #no_of bookings_hotel
            elif i == 8:
                X[j][i] = df['hotelname'].value_counts()[booking['hotelname']]

            #implement
            #no of cancellations
            elif i == 9:
                X[j][i] = 0

            #amount
            elif i == 10:
                X[j][i] = booking['amount']

            #hour of day of check in, we consider it important
            elif i == 11:
                X[j][i] = utils.get_hour_of_day(booking['checkin'])

            # day of week of check in, we consider it important
            elif i == 12:
                X[j][i] = utils.get_day_of_wk(booking['checkin'])

            # duration of stay, we consider it important
            elif i == 13:
                X[j][i] = utils.get_duration_seconds(booking['checkin'], booking['checkout'])

            # cab service required
            elif i == 14:
                X[j][i] = booking['cab_service_req']

            # number of rooms
            elif i == 15:
                X[j][i] = booking['number_of_rooms']
            elif i == 16:
                X[j][i] = booking['is_phone_booking']

            #hotel state
            elif i == 17:
                #calculate % occurence of the current hotel state in the hotelstates
                X[j][i] = df['hotel_state'].value_counts()[booking['hotel_state']] / float(len(df['hotel_state']))

        y[j] = booking['cancelled']




    return X, y


def split_data(rawX, rawY):
    n_samples = len(rawX)


    X = rawX
    y = rawY

    #pick 10% for testing
    train_ratio = 0.9;

    index = int(train_ratio * n_samples);

    X_train = X[:index]
    y_train = y[:index]

    X_test = X[index :]
    y_test = y[index :]

    return X_train, y_train, X_test, y_test
